class RPNCalculator
  # TODO: your code goes here!
  def initialize
    @arr = []
    @value = true
  end

  attr_accessor :arr, :value

  def push(num)
    @arr = arr << num
  end

  def plus
    raise 'calculator is empty' if arr.empty?
    @value = arr[-2..-1].reduce(:+)
    @arr[-2..-1] = value
  end

  def minus
    raise 'calculator is empty' if arr.empty?
    @value = arr[-2..-1].reduce(:-)
    @arr[-2..-1] = value
  end

  def divide
    raise 'calculator is empty' if arr.empty?
    @value = arr[-2..-1].map(&:to_f).reduce(:/)
    @arr[-2..-1] = value
  end

  def times
    raise 'calculator is empty' if arr.empty?
    @value = arr[-2..-1].reduce(:*)
    @arr[-2..-1] = value
  end

  def tokens(str)
    result = []
    str.delete!(' ')
    str.chars.each do |ch|
      if '0123456789'.include?(ch)
        result << ch.to_f
      else
        result << ch.to_sym
      end
    end
    result
  end

  def evaluate(str)
    array = tokens(str)
    i = 0
    while i < array.length
      if array[i].is_a? Symbol
        array[i-2..i] = array[i-2..i-1].reduce(array[i])
        i -= 2
      end
      i += 1
    end
    array[0]
  end
end
